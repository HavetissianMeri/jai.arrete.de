<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 */
class Role
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $create_publication;

    /**
     * @ORM\Column(type="boolean")
     */
    private $read_publication;

    /**
     * @ORM\Column(type="boolean")
     */
    private $update_publication;

    /**
     * @ORM\Column(type="boolean")
     */
    private $delete_publication;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatePublication(): ?bool
    {
        return $this->create_publication;
    }

    public function setCreatePublication(bool $create_publication): self
    {
        $this->create_publication = $create_publication;

        return $this;
    }

    public function getReadPublication(): ?bool
    {
        return $this->read_publication;
    }

    public function setReadPublication(bool $read_publication): self
    {
        $this->read_publication = $read_publication;

        return $this;
    }

    public function getUpdatePublication(): ?bool
    {
        return $this->update_publication;
    }

    public function setUpdatePublication(bool $update_publication): self
    {
        $this->update_publication = $update_publication;

        return $this;
    }

    public function getDeletePublication(): ?bool
    {
        return $this->delete_publication;
    }

    public function setDeletePublication(bool $delete_publication): self
    {
        $this->delete_publication = $delete_publication;

        return $this;
    }
}
